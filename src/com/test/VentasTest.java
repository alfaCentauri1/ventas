package com.test;

import com.ventas.Producto;
import com.ventas.Orden;

public class VentasTest {
    public static void main(String args[]){
        Producto producto1 = new Producto("Camisa", 15.00);
        Producto producto2 = new Producto("Camisa", 35.00);
        //
        Orden orden1 = new Orden();
        orden1.agregarProducto(producto1);
        orden1.agregarProducto(producto2);
        orden1.mostrarOrden();
    }
}
