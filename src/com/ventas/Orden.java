package com.ventas;

public class Orden {
    private int idOrden;
    private Producto productos[];
    private static int contadorOrdenes;
    public static final int MAX_PRODUCTOS = 10;
    private int cantidadProductos;

    public Orden(){
        this.idOrden = ++Orden.contadorOrdenes;
        this.productos = new Producto[Orden.MAX_PRODUCTOS];
    }

    public void agregarProducto(Producto producto){
        if (cantidadProductos >=0 && cantidadProductos < Orden.MAX_PRODUCTOS) {
            this.productos[cantidadProductos++] = producto;
        }
        else {
            System.out.println("Se ha superado el limite de productos: " + Orden.MAX_PRODUCTOS);
        }
    }

    public double calcularTotal(){
        double total = 0;
        for (int i=0; i < this.cantidadProductos; i++){
            Producto producto = this.productos[i];
            total += producto.getPrecio();
        }
        return total;
    }

    public void mostrarOrden(){
        System.out.println( "Id Orden: " + this.idOrden );
        double totalOrden = this.calcularTotal();
        System.out.println("Total de la Orden: $" + totalOrden);
        System.out.println("Productos de la orden: ");
        for(int i = 0; i < this.cantidadProductos; i++){
            System.out.println(this.productos[i]);
        }
    }
}
